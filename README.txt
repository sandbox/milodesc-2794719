INTRODUCTION
------------

If pages on your site contain links to untrusted sites and those links open in a new window, your users may be vulnerable to phishing attacks. A full explanation of this vulnerability is available here (https://mathiasbynens.github.io/rel-noopener/) and here (https://www.jitbit.com/alexblog/256-targetblank---the-most-underestimated-vulnerability-ever/). The long and short of it is that links opened in a new window are granted some access to the originating window. Therefore, if your site links to an external site and that site is opened in a new window (eg. via a 'target="_blank"'), the external site could, for example, redirect the window your site was open in to a phishing page that looks like the login screen for your site.

To prevent this, all links on your site that point to untrusted sites and open in a new window should have a rel attribute that contains at a minimum "noopener" and "noreferrer".

There is also a performance improvement that can result from adding these values to the rel attribute on your links (See: https://jakearchibald.com/2016/performance-benefits-of-rel-noopener/).

The goal of this module is to provide a suite of tools that will help mitigate the risks of this type of vulnerability.

 To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2794719


REQUIREMENTS
------------

This module does not have any special requirements.


INSTALLATION
------------

 * Download and install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Configure the module at Administration » Configuration » Content authoring (/admin/config/content/external_link_noopenizer).

 * Assign module permissions judiciously.


MAINTAINERS
-----------

Current maintainers:
 * Patrick Jensen (milodesc) - https://www.drupal.org/u/milodesc
