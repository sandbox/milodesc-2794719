/**
 * @file
 * Fix or displays warnings about external links on the page which don't have the noopener rel tag.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.external_link_noopenizer = {
    attach: function (context, settings) {

    // Before doing anything, let's make sure the module is configured to take some actions via JS.
    if (typeof settings.external_link_noopenizer !== 'undefined' &&
      (settings.external_link_noopenizer.fixViaJS == "1" || settings.external_link_noopenizer.displayWarnings == "1")) {
        $('a[target]', context).each(function () {
          var anchorElement = $(this);
          var targetAttribute = anchorElement.attr("target");
          var relAttribute = anchorElement.attr("rel").split(/\s+/);
          if (settings.external_link_noopenizer.trustedTargets.indexOf(targetAttribute) == -1 &&
              settings.external_link_noopenizer.trustedDomains.indexOf(this.hostname) == -1 &&
              (relAttribute.indexOf('noopener') == -1 || relAttribute.indexOf('noreferrer') == -1)) {

            // Found a vulnerable link, either fix or show warning
            if (settings.external_link_noopenizer.fixViaJS == "1") {
              var newRelAttribute = anchorElement.attr("rel");
              if (relAttribute.indexOf('noopener') == -1) {
                var noopener = "noopener";
                if (relAttribute[0] !== "") {
                  noopener = " " + noopener;
                }
                newRelAttribute = newRelAttribute + noopener;
              }
              if (relAttribute.indexOf('noreferrer') == -1) {
                newRelAttribute = newRelAttribute + " noreferrer";
              }
              anchorElement.attr("rel", newRelAttribute);
            }
            else if (settings.external_link_noopenizer.displayWarnings == "1" && $("body.external-link-noopenizer-warning-processed .external-link-noopenizer-warning-message").length == 0) {
              $("body.external-link-noopenizer-warning-processed").append($('<div class="external-link-noopenizer-warning-message">There are one or more links on this page that make users vulnerable to phishing attacks.</div>'));
            }
          }
        });
      }
    }
  };
}(jQuery));
