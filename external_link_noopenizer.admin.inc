<?php

/**
 * @file
 * Admin form for the External Link Noopenizer module.
 */

/**
 * Form builder function for module settings.
 */
function external_link_noopenizer_settings() {
  $form['external_link_noopenizer_warning'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display warnings'),
    '#description' => t('Display a warning on pages where there are one or more links that: 1) Open an untrusted site, 2) Open in a new window and 3) Do not have noopener and noreferrer set in a rel attribute. This functionality requires JavaScript to be turned on.'),
    '#default_value' => variable_get('external_link_noopenizer_warning', 0),
  );

  $form['external_link_noopenizer_js_fix'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fix offending links via JavaScript'),
    '#description' => t('By activating this option, a small script will be run on each page which will fix vulnerable links by adding noopener and noreferrer to the rel attribute on each vulnerable link.<br />This option will resolve the security risk for nearly all situations.'),
    '#default_value' => variable_get('external_link_noopenizer_js_fix', 0),
  );

  $form['external_link_noopenizer_trusted_domains'] = array(
    '#type' => 'textarea',
    '#title' => t('Trusted domains'),
    '#default_value' => variable_get('external_link_noopenizer_trusted_domains', ''),
    '#description' => t('Links going to domains in this list (eg. subdomain.yourdomain.com) will not have the noopener and noreferrer rel attributes added to them.'),
  );

  return system_settings_form($form);
}
